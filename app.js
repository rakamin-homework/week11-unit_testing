const express = require('express')
const app = express()
const todoController = require('./controllers/todo.controller')

app.use(express.json())
app.use('/todo', todoController)

app.get('/ping', (req, res ) => {
  res.json({ ping: true})
})

module.exports = app