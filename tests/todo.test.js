const request = require("supertest");
const assert = require("assert");
const todoController = require("../controllers/todo.controller");
const dotenv = require("dotenv");
dotenv.config();

const app = require("../app");

describe("GET /ping", () => {
  it("should return 200 ok", () => {
    request(app)
      .get("/ping")
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200)
      .end(function (err, res) {
        if (err) throw err;
      });
  });
});

// GET /api/todos
describe("GET /todo", () => {
  it("should return 200 ok", () => {
    request(app)
      .get("/todo")
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200);
  });
});

// GET /todo/:id
describe('GET /todo/:id', () => {
  it('should return 201 created', () => {
    request(app)
    .post('/todo')
    .send({title: 'Ngoding'})
    .set('Accept', 'application/json')
    .expect('Content-Type','application/json; charset=utf-8')
    .expect(201)

    request(app)
    .get(`/todo/4`)
    .expect('Content-Type','application/json; charset=utf-8')
    .expect(200)
  })

  it('should return 404 not found', () => {
    request(app)
    .get(`/todo/1000`)
    .expect('Content-Type','application/json; charset=utf-8')
    .expect(404)
  })
})

// POST api/todos
describe("POST /todo", () => {
  it("should return 201 ok", () => {
    request(app)
      .post("/todo")
      .send({ title: "Ngoding" })
      .set("Accept", "application/json")
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(201);
  });
});

// PUT /todo/:id
describe("PUT /todo/:id", () => {
  it("should return 200 ok", () => {
    request(app)
      .put("/todo/1")
      .send({ title: "Ngoding" })
      .set("Accept", "application/json")
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200);
  });
});

// DELETE /api/todos/:id
describe("DELETE /todo", () => {
  it("should return 200 ok", () => {
    request(app)
      .delete("/todo/21")
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200);
  });
});
