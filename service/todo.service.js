const todoRepository = require('../repository/todo.repository')

const findAllTodos = async () => {
    return await todoRepository.findAll()
}

const findTodoById = async (id) => {
    return await todoRepository.findById(id)
}

const createTodo = async (req) => {
    const todo = await todoRepository.save(req)

    return todo
}

const updateTodo = async (id, req) => {
    await todoRepository.findById(id)

    return await todoRepository.updateById(id, req)
}

const deleteTodo = async (id) => {
    await todoRepository.findById(id)

    return await todoRepository.deleteById(id)
}

module.exports = { findAllTodos, findTodoById, createTodo, updateTodo, deleteTodo }