const express = require("express");
const router = express.Router();
const todoService = require("../service/todo.service");

// POST
router.post("/", async (req, res) => {
  const result = await todoService.createTodo(req.body);

  if (!result)
    return res
      .status(422)
      .json({ message: "Failed create todo. Please try again" });

  return res.status(201).json({ message: "Data added.", data: result });
});

// GET
router.get("/", async (req, res) => {
  const result = await todoService.findAllTodos();

  return res.status(200).json({ data: result });
});

// GET by ID
router.get("/:id", async (req, res) => {
  const result = await todoService.findTodoById(req.params.id);

  if (!result) return res.status(404).json({ message: "Data not found." });

  return res.status(200).json({ data: result });
});

// PUT by ID
router.put("/:id", async (req, res) => {
  const result = await todoService.updateTodo(req.params.id, req.body);

  if (!result) return res.status(404).json({ message: "Data not found." });

  return res.status(200).json({ message: "Data updated." });
});

// DELETE by ID
router.delete("/:id", async (req, res) => {
  const result = await todoService.deleteTodo(req.params.id);

  if (!result) return res.status(404).json({ message: "Data not found." });

  return res.status(200).json({ message: "Data deleted." });
});

module.exports = router;
