1. npm install
2. config your db in config/config.json
3. change environment db in docker-compose same as config.json
4. try unit testing by run this command "npm test" or "npx jest"
5. open docker desktop
6. create images by run "docker build -t <your-images> ."
7. run this command to build database in docker "docker compose run --rm web-server npx sequelize-cli db:migrate"
8. start your container with this command "docker compose up -d"
9. try all endpoints in postman
